package sorting

import "testing"

func TestMergeSorter(t *testing.T) {
    sorter := new(MergeSorter)

    input := []int{3}
    output := []int{3}


    sorted := sorter.Sort(input)

    if len(sorted) != len(output) {
        t.Errorf("Invalid output length %v, want %v", len(sorted), len(output))
    }
    for i, x := range sorted {
        if x != output[i] {
            t.Errorf("Invalid output %v, want %v", sorted, output)
            break
        }
    }

    input = []int{2, 1}
    output = []int{1, 2}


    sorted = sorter.Sort(input)

    if len(sorted) != len(output) {
        t.Errorf("Invalid output length %v, want %v", len(sorted), len(output))
    }
    for i, x := range sorted {
        if x != output[i] {
            t.Errorf("Invalid output %v, want %v", sorted, output)
            break
        }
    }

    input = []int{3, 3, 2, 1, 3, 6, 5, 4}
    output = []int{1, 2, 3, 3, 3, 4, 5, 6}

    sorted = sorter.Sort(input)

    if len(sorted) != len(output) {
        t.Errorf("Invalid output length %v, want %v", len(sorted), len(output))
    }
    for i, x := range sorted {
        if x != output[i] {
            t.Errorf("Invalid output %v, want %v", sorted, output)
            break
        }
    }

    input = []int{3, 3, 2, 1, 3, 6, 5, 4, 1}
    output = []int{1, 1, 2, 3, 3, 3, 4, 5, 6}

    sorted = sorter.Sort(input)

    if len(sorted) != len(output) {
        t.Errorf("Invalid output length %v, want %v", len(sorted), len(output))
    }
    for i, x := range sorted {
        if x != output[i] {
            t.Errorf("Invalid output %v, want %v", sorted, output)
            break
        }
    }
}
