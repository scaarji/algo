package sorting

import "math"

type MergeSorter struct {}

func (sorter MergeSorter) merge(right, left []int) []int {
	leftIndex := 0
	rightIndex := 0

	leftLength := len(left)
	rightLength := len(right)
	length := leftLength + rightLength

	result := make([]int, 0, length)

	for i := 0; i < length; i += 1 {
		if left[leftIndex] > right[rightIndex] {
			result = append(result, right[rightIndex])
			rightIndex += 1
		} else {
			result = append(result, left[leftIndex])
			leftIndex += 1
		}

		if leftIndex == leftLength {
			result = append(result, right[rightIndex:]...)
			break
		}
		if rightIndex == rightLength {
			result = append(result, left[leftIndex:]...)
			break
		}

	}

	return result
}

func (sorter MergeSorter) Sort(list []int) []int {
	length := len(list)
	if length == 1 {
		return list
	}
	half := int(math.Ceil(float64(length) / 2))
	leftHalf := sorter.Sort(list[:half])
	rightHalf := sorter.Sort(list[half:])

	return sorter.merge(leftHalf, rightHalf)
}
