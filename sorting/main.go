package sorting

import "fmt"

type Sorter interface {
    Sort(list []int) []int
}

func main() {
	sorter := new(MergeSorter)
	fmt.Println("%v", sorter.Sort([]int{3, 3, 2, 1, 3, 4, 5, 6}))
	fmt.Println("%v", sorter.Sort([]int{3, 2, 1, 3, 4, 5, 6}))
}