package sorting

import "math"

type QuickSorter struct {}

func (sorter QuickSorter) choosePivot(list []int, left, right int) int {
    middle := int(math.Ceil(float64(left + (right - left) / 2)))
    if list[left] <= list[right] {
        // left...right
        if list[middle] >= list[right] {
            // left...middle...right
            return right
        } else if list[middle] <= list[left] {
            // middle...left...right
            return left
        } else {
            // left...middle...right
            return middle
        }
    } else {
        // right...left
        if list[middle] <= list[right] {
            // middle...right...left
            return right
        } else if list[left] <= list[middle] {
            // right...left...middle
            return left
        } else {
            // right...middle...left
            return middle
        }
    }
}

func (sorter QuickSorter) partition(list []int, left, right int) int {
    pivotIndex := sorter.choosePivot(list, left, right)
    pivotValue := list[pivotIndex]
    list[right], list[pivotIndex] = list[pivotIndex], list[right]
    boundary := left
    for i := left; i < right; i += 1 {
        if list[i] < pivotValue {
            list[boundary], list[i] = list[i], list[boundary]
            boundary += 1
        }
    }
    list[right], list[boundary] = list[boundary], list[right]

    return boundary
}

func (sorter QuickSorter) sort(list []int, from, to int) {
    if from >= to {
        return
    }
    pivot := sorter.partition(list, from, to)
    sorter.sort(list, from, pivot - 1)
    sorter.sort(list, pivot + 1, to)
}

func (sorter QuickSorter) Sort(list []int) []int {
    result := make([]int, len(list))
    copy(result, list)
    sorter.sort(result, 0, len(list) - 1)
    return result
}
