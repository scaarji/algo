package adjacencyList

type Vertex struct {
    Id int
}

type Edge struct {
    Id int
    Weight int
}

type EdgeEnds struct {
    From, To int
}

type AdjacencyList struct {
    Vertices []Vertex
    Edges []Edge
    EdgeEnds map[int]EdgeEnds
    VertexEdges map[int][]int
}

func (list *AdjacencyList) AddVertexEdge (vertex *Vertex, edge *Edge) {
    list.VertexEdges[vertex.Id] = append(list.VertexEdges[vertex.Id], edge.Id)
}

func (list *AdjacencyList) AddEdge (edge Edge, fromId, toId int) *Edge {
    from := list.GetVertex(fromId)
    to := list.GetVertex(toId)

    edge.Id = len(list.Edges)

    list.Edges = append(list.Edges, edge)
    list.EdgeEnds[edge.Id] = EdgeEnds{from.Id, to.Id}

    list.AddVertexEdge(from, &edge)
    list.AddVertexEdge(to, &edge)

    return &edge
}

func (list *AdjacencyList) AddVertex (vertex Vertex) *Vertex {
    vertex.Id = len(list.Vertices)
    list.Vertices = append(list.Vertices, vertex)
    list.VertexEdges[vertex.Id] = make([]int, 0)

    return &vertex
}

func (list AdjacencyList) GetEdgeEnds (edge Edge) EdgeEnds {
    return list.EdgeEnds[edge.Id]
}

func (list AdjacencyList) GetVertex (id int) *Vertex {
    if id < 0 || id >= len(list.Vertices) {
        return nil
    }
    return &list.Vertices[id]
}

func (list AdjacencyList) GetVertexEdges (vertex *Vertex) []*Edge {
    edges := make([]*Edge, len(list.VertexEdges[vertex.Id]))
    for i, edgeId := range list.VertexEdges[vertex.Id] {
        edges[i] = &list.Edges[edgeId]
    }
    return edges
}

func (list AdjacencyList) GetConnectingEdge (from, to int) (*Edge, bool) {
    vertexFrom := list.GetVertex(from)
    vertexTo := list.GetVertex(to)

    if vertexFrom == nil || vertexTo == nil {
        return nil, false
    }

    for _, edgeId := range list.VertexEdges[vertexFrom.Id] {
        edge := list.Edges[edgeId]
        edgeEnds := list.GetEdgeEnds(edge)
        if edgeEnds.To == vertexTo.Id {
            return &edge, true
        }
    }

    return nil, false
}

func BuildAdjacencyList () AdjacencyList {
    list := AdjacencyList{
        Vertices: make([]Vertex, 0),
        Edges: make([]Edge, 0),
        EdgeEnds: make(map[int]EdgeEnds),
        VertexEdges: make(map[int][]int),
    }
    return list
}
