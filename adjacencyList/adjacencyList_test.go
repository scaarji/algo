package adjacencyList

import "testing"

func TestAdjucencyList(t *testing.T) {
    list := BuildAdjacencyList()
    if len(list.Vertices) != 0 {
        t.Errorf("Empty vertex list should be initialized")
        return
    }
    if len(list.Edges) != 0 {
        t.Errorf("Empty edge list should be initialized")
        return
    }
}

func TestAdjucencyList_AddVertex (t *testing.T) {
    list := BuildAdjacencyList()
    vertex := list.AddVertex(Vertex{})

    if len(list.Vertices) != 1 {
        t.Errorf("Vertex not added, list of vertices has len %v", len(list.Vertices))
        return
    }
    if list.Vertices[0].Id != 0 {
        t.Errorf("Invalid vertex Id %v generated, expected %v", list.Vertices[0].Id, 0)
        return
    }
    _, exists := list.VertexEdges[0]
    if exists == false {
        t.Errorf("Vertex edges mapping was not created")
        return
    }

    vertex = list.AddVertex(Vertex{})

    if len(list.Vertices) != 2 {
        t.Errorf("Vertex not added, list of vertices has len %v", len(list.Vertices))
        return
    }
    if list.Vertices[1].Id != 1 {
        t.Errorf("Invalid vertex Id %v generated, expected %v", list.Vertices[1].Id, 1)
        return
    }
    _, exists = list.VertexEdges[vertex.Id]
    if exists == false {
        t.Errorf("Vertex edges mapping was not created")
        return
    }
}

func TestAdjucencyList_AddVertexEdge (t *testing.T) {
    var (
        list AdjacencyList
        vertex Vertex
        edge Edge
    )

    list = BuildAdjacencyList()
    vertex = *list.AddVertex(Vertex{})

    edge = Edge{Id: 0, Weight: 10}
    list.AddVertexEdge(&vertex, &edge)

    if len(list.VertexEdges[vertex.Id]) != 1 {
        t.Errorf("Edge not added to vertex edges")
        return
    }
    if list.VertexEdges[vertex.Id][0] != edge.Id {
        t.Errorf("Invalid edge %v added, expected %v", list.VertexEdges[vertex.Id][0], edge.Id)
        return
    }

    edge = Edge{Id: 30, Weight: -12310}
    list.AddVertexEdge(&vertex, &edge)

    if len(list.VertexEdges[vertex.Id]) != 2 {
        t.Errorf("Edge not added to vertex edges")
        return
    }
    if list.VertexEdges[vertex.Id][1] != edge.Id {
        t.Errorf("Invalid edge %v added, expected %v", list.VertexEdges[vertex.Id][0], edge.Id)
        return
    }
}


func TestAdjucencyList_AddEdge (t *testing.T) {
    list := BuildAdjacencyList()
    vertex1 := list.AddVertex(Vertex{})
    vertex2 := list.AddVertex(Vertex{})
    vertex3 := list.AddVertex(Vertex{})

    edge := list.AddEdge(Edge{Weight: 100}, vertex1.Id, vertex2.Id)
    if len(list.Edges) != 1 {
        t.Errorf("Edge not added, list of edges has len %v", len(list.Edges))
        return
    }
    if list.Edges[0].Id != 0 {
        t.Errorf("Invalid edge Id %v generated, expected %v", list.Edges[0].Id, 0)
        return
    }
    if len(list.VertexEdges[vertex1.Id]) != 1 {
        t.Errorf("Edge not added to vertex edges of vertex %v", vertex1.Id)
        return
    }
    if len(list.VertexEdges[vertex2.Id]) != 1 {
        t.Errorf("Edge not added to vertex edges of vertex %v", vertex2.Id)
        return
    }
    _, exists := list.EdgeEnds[edge.Id]
    if !exists {
        t.Errorf("Edge ends not created")
        return
    }
    if list.EdgeEnds[edge.Id].From != vertex1.Id {
        t.Errorf("Invalid vertex added to edge ends %v, expected %v", list.EdgeEnds[edge.Id].From, vertex1.Id)
    }
    if list.EdgeEnds[edge.Id].To != vertex2.Id {
        t.Errorf("Invalid vertex added to edge ends %v, expected %v", list.EdgeEnds[edge.Id].To, vertex2.Id)
    }

    _ = list.AddEdge(Edge{Weight: -123}, vertex2.Id, vertex3.Id)
    if len(list.Edges) != 2 {
        t.Errorf("Edge not added, list of edges has len %v", len(list.Edges))
        return
    }
    if list.Edges[1].Id != 1 {
        t.Errorf("Invalid edge Id %v generated, expected %v", list.Edges[1].Id, 1)
        return
    }
    if len(list.VertexEdges[vertex3.Id]) != 1 {
        t.Errorf("Edge not added to vertex edges of vertex %v", vertex3.Id)
        return
    }
    if len(list.VertexEdges[vertex2.Id]) != 2 {
        t.Errorf("Edge not added to vertex edges of vertex %v", vertex2.Id)
        return
    }
}

func TestAdjucencyList_GetVertex(t *testing.T) {
    list := BuildAdjacencyList()
    vertex := *list.AddVertex(Vertex{})

    v := *list.GetVertex(vertex.Id)
    if v != vertex {
        t.Errorf("Invalid vertex returned %v, expected %v", v.Id, vertex.Id)
    }

    nonExistantVertex := list.GetVertex(6)
    if nonExistantVertex != nil {
        t.Errorf("Should return nil if non existant vertex id passed")
    }
}

func TestAdjucencyList_GetVertexEdges(t *testing.T) {
    list := BuildAdjacencyList()
    vertex := list.AddVertex(Vertex{})
    vertex1 := list.AddVertex(Vertex{})
    vertex2 := list.AddVertex(Vertex{})

    list.AddEdge(Edge{Weight: 100}, vertex.Id, vertex1.Id)
    list.AddEdge(Edge{Weight: -10}, vertex.Id, vertex2.Id)

    var edges []*Edge
    edges = list.GetVertexEdges(vertex)
    if len(edges) != 2 {
        t.Errorf("Invalid number of edges returned %v, expected %v", len(edges), 2)
        return
    }
}

func TestAdjucencyList_GetEdgeEnds(t *testing.T) {
    list := BuildAdjacencyList()
    vertex1 := list.AddVertex(Vertex{})
    vertex2 := list.AddVertex(Vertex{})

    edge := *list.AddEdge(Edge{Weight: 100}, vertex1.Id, vertex2.Id)

    var edgeEnds EdgeEnds = list.GetEdgeEnds(edge)
    if edgeEnds.From != vertex1.Id {
        t.Errorf("Invalid vertex added to edge ends %v, expected %v", edgeEnds.From, vertex1.Id)
    }
    if edgeEnds.To != vertex2.Id {
        t.Errorf("Invalid vertex added to edge ends %v, expected %v", edgeEnds.To, vertex2.Id)
    }
}

func TestAdjucencyList_GetConnectingEdge(t *testing.T) {
    list := BuildAdjacencyList()
    vertex1 := list.AddVertex(Vertex{})
    vertex2 := list.AddVertex(Vertex{})
    vertex3 := list.AddVertex(Vertex{})

    edge := list.AddEdge(Edge{Weight: 100}, vertex1.Id, vertex2.Id)

    var (
        res *Edge
        exists bool
    )
    res, exists = list.GetConnectingEdge(vertex1.Id, vertex2.Id)
    if exists != true || res == nil || res.Id != edge.Id {
        t.Errorf("Expected edge from %v to %v to exist", vertex1.Id, vertex2.Id)
    }
    res, exists = list.GetConnectingEdge(vertex2.Id, vertex1.Id)
    if exists != false || res != nil {
        t.Errorf("Expected edge from %v to %v to not exist", vertex2.Id, vertex1.Id)
    }
    res, exists = list.GetConnectingEdge(vertex1.Id, vertex3.Id)
    if exists != false || res != nil {
        t.Errorf("Expected edge from %v to %v to not exist", vertex1.Id, vertex3.Id)
    }
    res, exists = list.GetConnectingEdge(vertex1.Id, 6)
    if exists != false || res != nil {
        t.Errorf("Expected edge for unexistant vertex to not exist")
    }
}
