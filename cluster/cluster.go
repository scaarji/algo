package cluster

import "sort"
import al "bitbucket.org/scaarji/adjacencyList"
import uf "bitbucket.org/scaarji/unionFind"

type edgesList []al.Edge

func (e edgesList) Len() int {
    return len(e)
}
func (e edgesList) Less(i, j int) bool {
    return e[i].Weight < e[j].Weight
}
func (e edgesList) Swap(i, j int) {
    e[i], e[j] = e[j], e[i]
}

func Cluster (list al.AdjacencyList, requiredClastersCnt int) (clusters [][]al.Vertex, minSpacing int) {
    edges := make(edgesList, len(list.Edges))
    for i, edge := range list.Edges {
        edges[i] = edge
    }

    sort.Sort(edges)

    clusterMap := make(map[int]*uf.Set)
    clustersCnt := len(list.Vertices)

    for _, vertex := range list.Vertices {
        clusterMap[vertex.Id] = uf.MakeSet(vertex.Id)
    }

    edgeIndex := 0
    edgeLen := len(edges)
    for clustersCnt > requiredClastersCnt && edgeIndex < edgeLen {
        edge := edges[edgeIndex]
        edgeIndex += 1

        edgeEnds := list.GetEdgeEnds(edge)
        xRoot := uf.Find(clusterMap[edgeEnds.From])
        yRoot := uf.Find(clusterMap[edgeEnds.To])

        if xRoot != yRoot {
            uf.Union(xRoot, yRoot)
            clustersCnt -= 1
        }
    }

    for _, edge := range edges {
        edgeEnds := list.GetEdgeEnds(edge)
        xRoot := uf.Find(clusterMap[edgeEnds.From])
        yRoot := uf.Find(clusterMap[edgeEnds.To])
        if xRoot != yRoot {
            minSpacing = edge.Weight
            break
        }
    }

    clusters = make([][]al.Vertex, 0)

    indexMap := make(map[int]int)
    indexCounter := 0
    for _, vertex := range list.Vertices {
        root := uf.Find(clusterMap[vertex.Id])
        id := root.Data.(int)

        counter, exists := indexMap[id]
        if !exists {
            counter = indexCounter
            indexMap[id] = indexCounter
            indexCounter += 1
            clusters = append(clusters, make([]al.Vertex, 0))
        }
        clusters[counter] = append(clusters[counter], vertex)
    }

    return
}
