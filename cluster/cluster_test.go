package cluster

import (
    "testing"
    al "bitbucket.org/scaarji/adjacencyList"
)

type Distance struct{from, to, weight int}

type TestCase struct {
    nodesCnt int
    clustersCnt int
    distances []Distance
    result int
}


func TestCluster (t *testing.T) {
    testCases := []TestCase{
        TestCase{
            nodesCnt: 10,
            clustersCnt: 4,
            distances: []Distance{
                {1, 2, 134365},
                {1, 3, 847434},
                {1, 4, 763775},
                {1, 5, 255070},
                {1, 6, 495436},
                {1, 7, 449492},
                {1, 8, 651593},
                {1, 9, 788724},
                {1, 10, 93860},
                {2, 3, 28348},
                {2, 4, 835766},
                {2, 5, 432768},
                {2, 6, 762281},
                {2, 7, 2107},
                {2, 8, 445388},
                {2, 9, 721541},
                {2, 10, 228763},
                {3, 4, 945271},
                {3, 5, 901428},
                {3, 6, 30590},
                {3, 7, 25446},
                {3, 8, 541413},
                {3, 9, 939150},
                {3, 10, 381205},
                {4, 5, 216600},
                {4, 6, 422117},
                {4, 7, 29041},
                {4, 8, 221692},
                {4, 9, 437888},
                {4, 10, 495813},
                {5, 6, 233085},
                {5, 7, 230867},
                {5, 8, 218782},
                {5, 9, 459604},
                {5, 10, 289782},
                {6, 7, 21490},
                {6, 8, 837578},
                {6, 9, 556455},
                {6, 10, 642295},
                {7, 8, 185907},
                {7, 9, 992544},
                {7, 10, 859947},
                {8, 9, 120890},
                {8, 10, 332696},
                {9, 10, 721485},
            },
            result: 134365,
        },
        TestCase{
            nodesCnt: 6,
            clustersCnt: 4,
            distances: []Distance{
                {1, 2, 1},
                {1, 3, 3},
                {1, 4, 8},
                {1, 5, 12},
                {1, 6, 13},
                {2, 3, 2},
                {2, 4, 14},
                {2, 5, 11},
                {2, 6, 10},
                {3, 4, 15},
                {3, 5, 17},
                {3, 6, 16},
                {4, 5, 7},
                {4, 6, 19},
                {5, 6, 9},
            },
            result: 7,
        },
        TestCase{
            nodesCnt: 6,
            clustersCnt: 0,
            distances: []Distance{
                {1, 2, 1},
                {1, 3, 3},
                {1, 4, 8},
                {1, 5, 12},
                {1, 6, 13},
                {2, 3, 2},
                {4, 5, 7},
                {5, 6, 9},
            },
            result: 0,
        },
    }
    for testCaseIndex, testCase := range testCases {
        list := al.BuildAdjacencyList()
        for i := 0; i < testCase.nodesCnt; i += 1 {
            list.AddVertex(al.Vertex{})
        }
        for _, edge := range testCase.distances {
            list.AddEdge(al.Edge{Weight: edge.weight}, edge.from - 1, edge.to - 1)
        }

        clusters, result := Cluster(list, testCase.clustersCnt)
        if testCase.clustersCnt != 0 && len(clusters) != testCase.clustersCnt {
            t.Errorf("Test case %v failed: Invalid number of clusters %v, expected %v", testCaseIndex, len(clusters), testCase.clustersCnt)
        }
        if testCase.result != 0 && result != testCase.result {
            t.Errorf("Test case %v failed: Invalid result %v, expected %v", testCaseIndex, result, testCase.result)
        }
    }
}
