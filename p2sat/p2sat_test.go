package p2sat

import "testing"

type testCase struct {
    number int
    conditions []Condition
    result bool
}

func TestP2sat(t *testing.T) {
    testCases := []testCase{
        testCase{
            number: 2,
            conditions: []Condition{
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: false,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: true,
                    },
                    Item{
                        Index: 1,
                        IsNeg: true,
                    },
                },
            },
            result: true,
        },
        testCase{
            number: 2,
            conditions: []Condition{
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: false,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: true,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
            },
            result: true,
        },
        testCase{
            number: 2,
            conditions: []Condition{
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: false,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: true,
                    },
                    Item{
                        Index: 1,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: false,
                    },
                    Item{
                        Index: 1,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: true,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
            },
            result: false,
        },
        testCase{
            number: 25,
            conditions: []Condition{
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: false,
                    },
                    Item{
                        Index: 1,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 1,
                        IsNeg: true,
                    },
                    Item{
                        Index: 5,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 7,
                        IsNeg: false,
                    },
                    Item{
                        Index: 8,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: false,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 11,
                        IsNeg: false,
                    },
                    Item{
                        Index: 2,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 5,
                        IsNeg: true,
                    },
                    Item{
                        Index: 7,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 5,
                        IsNeg: false,
                    },
                    Item{
                        Index: 6,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 7,
                        IsNeg: false,
                    },
                    Item{
                        Index: 12,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 7,
                        IsNeg: true,
                    },
                    Item{
                        Index: 8,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 8,
                        IsNeg: false,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 8,
                        IsNeg: true,
                    },
                    Item{
                        Index: 4,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 8,
                        IsNeg: false,
                    },
                    Item{
                        Index: 2,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 12,
                        IsNeg: false,
                    },
                    Item{
                        Index: 5,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 9,
                        IsNeg: true,
                    },
                    Item{
                        Index: 8,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: true,
                    },
                    Item{
                        Index: 9,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: true,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 3,
                        IsNeg: true,
                    },
                    Item{
                        Index: 4,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 11,
                        IsNeg: false,
                    },
                    Item{
                        Index: 7,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 12,
                        IsNeg: false,
                    },
                    Item{
                        Index: 3,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 12,
                        IsNeg: false,
                    },
                    Item{
                        Index: 4,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 12,
                        IsNeg: false,
                    },
                    Item{
                        Index: 5,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                    Item{
                        Index: 3,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                    Item{
                        Index: 6,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 3,
                        IsNeg: false,
                    },
                    Item{
                        Index: 12,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 12,
                        IsNeg: false,
                    },
                    Item{
                        Index: 4,
                        IsNeg: false,
                    },
                },
            },
            result: true,
        },
        testCase{
            number: 25,
            conditions: []Condition{
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: false,
                    },
                    Item{
                        Index: 1,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 1,
                        IsNeg: true,
                    },
                    Item{
                        Index: 5,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 7,
                        IsNeg: false,
                    },
                    Item{
                        Index: 8,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: false,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 11,
                        IsNeg: false,
                    },
                    Item{
                        Index: 2,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 5,
                        IsNeg: true,
                    },
                    Item{
                        Index: 7,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 5,
                        IsNeg: false,
                    },
                    Item{
                        Index: 6,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 7,
                        IsNeg: false,
                    },
                    Item{
                        Index: 12,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 7,
                        IsNeg: true,
                    },
                    Item{
                        Index: 8,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 8,
                        IsNeg: false,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 8,
                        IsNeg: true,
                    },
                    Item{
                        Index: 7,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 8,
                        IsNeg: false,
                    },
                    Item{
                        Index: 2,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 12,
                        IsNeg: false,
                    },
                    Item{
                        Index: 3,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 9,
                        IsNeg: true,
                    },
                    Item{
                        Index: 8,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: true,
                    },
                    Item{
                        Index: 9,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 0,
                        IsNeg: true,
                    },
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 3,
                        IsNeg: true,
                    },
                    Item{
                        Index: 4,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 11,
                        IsNeg: true,
                    },
                    Item{
                        Index: 7,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 12,
                        IsNeg: false,
                    },
                    Item{
                        Index: 3,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 12,
                        IsNeg: false,
                    },
                    Item{
                        Index: 3,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 12,
                        IsNeg: false,
                    },
                    Item{
                        Index: 5,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                    Item{
                        Index: 3,
                        IsNeg: false,
                    },
                },
                Condition{
                    Item{
                        Index: 1,
                        IsNeg: false,
                    },
                    Item{
                        Index: 6,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 3,
                        IsNeg: false,
                    },
                    Item{
                        Index: 12,
                        IsNeg: true,
                    },
                },
                Condition{
                    Item{
                        Index: 12,
                        IsNeg: true,
                    },
                    Item{
                        Index: 11,
                        IsNeg: true,
                    },
                },
            },
            result: false,
        },
    }

    for testCaseIndex, testCase := range testCases {
        result := Solve(testCase.number, testCase.conditions)
        if testCase.result != result {
            t.Errorf("Test case %v failed: Invalid result %v, expected %v", testCaseIndex, result, testCase.result)
            return
        }
    }
}
