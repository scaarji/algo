package p2sat

import "math"
import "math/rand"
import "time"

type Item struct {
    Index int
    IsNeg bool
}

type Condition [2]Item

func check (input []bool, conditions []Condition) (isCorrect bool, invalidIndex int) {
    var (
        comp1, comp2, result bool
        index int
        condition Condition
    )
    for index, condition = range conditions {
        comp1 = input[condition[0].Index]
        if condition[0].IsNeg {
            comp1 = !comp1
        }
        comp2 = input[condition[1].Index]
        if condition[1].IsNeg {
            comp2 = !comp2
        }
        result = comp1 || comp2
        if !result {
            return false, index
        }
    }
    return true, 0
}

func preprocess (conditions []Condition, predefines map[int]bool) ([]Condition, map[int]bool) {
    // previously found value of variable
    occurence := make(map[int]bool)
    // add index here if sign changes - skip in removal
    variable := make(map[int]bool)

    for _, condition := range conditions {
        for _, item := range condition {
            _, exists := predefines[item.Index]
            if exists {
                continue
            }
            prevValue, exists := occurence[item.Index]
            if !exists {
                occurence[item.Index] = item.IsNeg
                variable[item.Index] = false
            } else {
                if prevValue != item.IsNeg {
                    variable[item.Index] = true
                }
            }
        }
    }
    result := make([]Condition, 0)

    for _, condition := range conditions {
        shouldBeAdded := true
        for _, item := range condition {
            _, exists := predefines[item.Index]
            if exists {
                shouldBeAdded = false
                continue
            }
            isVariable := variable[item.Index]
            if !isVariable {
                shouldBeAdded = false
                predefines[item.Index] = !occurence[item.Index]
            }
        }
        if shouldBeAdded {
            result = append(result, condition)
        }
    }

    if len(result) != len(conditions) {
        return preprocess(result, predefines)
    } else {
        return result, predefines
    }
}

func Solve (number int, inputConditions []Condition) bool {
    repetitionsCnt := int(math.Ceil(math.Log2(float64(number))))
    changesCnt := 2 * number * number

    conditions, defaultInput := preprocess(inputConditions, make(map[int]bool))

    r := rand.New(rand.NewSource(time.Now().UnixNano()))

    var (
        i, j, invalidIndex, flipIndex int
        v, exists, isCorrect bool
        item Item
    )

    for i = 0; i < repetitionsCnt; i++ {
        input := make([]bool, number)
        for j = 0; j < number; j++ {
            v, exists = defaultInput[j]
            if exists {
                input[j] = v
            } else {
                input[j] = r.Intn(2) == 0
            }
        }
        for j = 0; j < changesCnt; j++ {
            isCorrect, invalidIndex = check(input, conditions)
            if isCorrect {
                return true
            }
            flipIndex = r.Intn(2)
            item = conditions[invalidIndex][flipIndex]

            if item.IsNeg {
                input[item.Index] = false
            } else {
                input[item.Index] = true
            }
        }
    }
    return false
}
