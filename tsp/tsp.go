package tsp

import "math"

type Set interface {
    Distance(i, j int) float32
    Len() int
}


func GetMinimumTour(set Set) float32 {
    length := set.Len()
    options := 2 << uint(length - 2)
    a := make([][]float32, options)

    var (
        i, j, k, m, sIndex, sSize int
        s []int
        min, value float32
    )

    for i = 0; i < options; i += 1 {
        a[i] = make([]float32, length)
        for j = 0; j < length; j += 1 {
            a[i][j] = float32(math.Inf(0))
        }
    }

    for m = 2; m <= length; m += 1 {
        for sIndex = 0; sIndex < options; sIndex += 1 {
            // will contain list of elements
            s = make([]int, m)
            s[0] = 0
            // will contain set size
            sSize = 1

            // check size and fill elements list
            for j = 1; j < length; j += 1 {
                if sIndex & (1 << uint(j - 1)) != 0 {
                    if sSize >= m {
                        sSize += 1
                        break
                    }
                    s[sSize] = j
                    sSize += 1
                }
            }
            if sSize != m {
                continue
            }

            for _, j = range s {
                if j == 0 {
                    continue
                }
                min = float32(math.Inf(0))
                for _, k = range s {
                    if k == j {
                        continue
                    }
                    offset := sIndex ^ (1 << uint(j - 1))
                    if offset == 0 {
                        if k == 0 {
                            value = set.Distance(j, k)
                        } else {
                            continue
                        }
                    } else {
                        value = a[offset][k] + set.Distance(j, k)
                    }
                    if value < min {
                        min = value
                    }
                }
                a[sIndex][j] = min
            }
        }
    }

    min = float32(math.Inf(0))
    for j = 1; j < length; j++ {
        value = a[options - 1][j] + set.Distance(j, 0)
        if value < min {
            min = value
        }
    }
    return min
}
