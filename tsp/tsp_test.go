package tsp

import "testing"
import "math"

type coordinates struct {
    x, y float64
}

type Case []coordinates

func (set Case) Distance(i, j int) float32 {
    res := math.Sqrt(math.Pow(set[i].x - set[j].x, 2) +
        math.Pow(set[i].y - set[j].y, 2))
    return float32(res)
}

func (set Case) Len() int {
    return len(set)
}

type TestCase struct {
    set Case
    result float64
}

func TestTsp(t *testing.T) {
    testCases := []TestCase{
        TestCase{
            set: Case{
                coordinates{0, 0},
                coordinates{1, 0},
                coordinates{0, 1},
                coordinates{1, 1},
            },
            result: 4,
        },
        TestCase{
            set: Case{
                coordinates{0, 2},
                coordinates{1, 0},
                coordinates{2, 0},
                coordinates{2, 1},
                coordinates{4, 2},
            },
            result: 10.4721,
        },
        TestCase{
            set: Case{
                coordinates{20833.3333, 17100.0000},
                coordinates{20900.0000, 17066.6667},
                coordinates{21300.0000, 13016.6667},
                coordinates{21600.0000, 14150.0000},
                coordinates{21600.0000, 14966.6667},
                coordinates{21600.0000, 16500.0000},
                coordinates{22183.3333, 13133.3333},
                coordinates{22583.3333, 14300.0000},
                coordinates{22683.3333, 12716.6667},
                coordinates{23616.6667, 15866.6667},
                coordinates{23700.0000, 15933.3333},
                coordinates{23883.3333, 14533.3333},
                coordinates{24166.6667, 13250.0000},
                coordinates{25149.1667, 12365.8333},
            },
            result: 16898.1,
        },
    }
    for testCaseIndex, testCase := range testCases {
        result := GetMinimumTour(testCase.set)
        if math.Abs(testCase.result - float64(result)) > 0.1 {
            t.Errorf("Test case %v failed: Invalid result %v, expected %v", testCaseIndex, result, testCase.result)
            return
        }
    }
}
