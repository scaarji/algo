package mst

import (
    "testing"
    al "bitbucket.org/scaarji/adjacencyList"
)


func TestCalculate (t *testing.T) {
    testCase := struct{
        VerticesCnt int
        Edges []struct{From, To, Weight int}
        Result int
    }{
        6,
        []struct{From, To, Weight int}{
            {0, 1, 2474},
            {1, 3, -246},
            {3, 2, 640},
            {3, 4, 2088},
            {2, 5, 4586},
            {5, 4, 3966},
            {4, 0, -3824},
        },
        2624,
    }
    list := al.BuildAdjacencyList()
    for i := 0; i < testCase.VerticesCnt; i += 1 {
        list.AddVertex(al.Vertex{})
    }
    for _, edge := range testCase.Edges {
        list.AddEdge(al.Edge{Weight: edge.Weight}, edge.From, edge.To)
    }
    _, result := Calculate(list)
    if result != testCase.Result {
        t.Errorf("Invalid result %v, expected %v", result, testCase.Result)
        return
    }
}
