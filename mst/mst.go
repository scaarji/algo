package mst

import (
    "container/heap"
    al "bitbucket.org/scaarji/adjacencyList"
)

type EdgeHeap []al.Edge

func (e EdgeHeap) Len() int {
    return len(e)
}

func (e EdgeHeap) Less(i, j int) bool {
    return e[i].Weight < e[j].Weight
}

func (e EdgeHeap) Swap(i, j int) {
    if i >= 0 && j >= 0 {
        e[i], e[j] = e[j], e[i]
    }
}

func (e *EdgeHeap) Push(x interface{}) {
    *e = append(*e, x.(al.Edge))
}

func (e *EdgeHeap) Pop() interface{} {
    old := *e
    n := len(old)
    x := old[n-1]
    *e = old[0 : n-1]
    return x
}

func Calculate (list al.AdjacencyList) (edges []al.Edge, sum int) {
    h := make(EdgeHeap, 0)
    heap.Init(&h)

    edges = make([]al.Edge, 0)
    vertices := make(map[int]bool)
    verticesLen := 0
    totalVerticesLen := len(list.Vertices)

    addVertex := func (vertex al.Vertex) {
        vertices[vertex.Id] = true
        verticesLen += 1
        edges := list.GetVertexEdges(&vertex)
        for _, edge := range edges {
            edgeEnds := list.GetEdgeEnds(*edge)
            if vertices[edgeEnds.From] == true && vertices[edgeEnds.To] == true {
                continue
            }
            heap.Push(&h, *edge)
        }
    }

    addVertex(list.Vertices[0])

    for verticesLen < totalVerticesLen {
        edge := heap.Pop(&h).(al.Edge)
        edgeEnds := list.GetEdgeEnds(edge)
        from, to := list.GetVertex(edgeEnds.From), list.GetVertex(edgeEnds.To)
        if vertices[from.Id] == true && vertices[to.Id] == true {
            continue
        }
        if vertices[from.Id] == true {
            addVertex(*to)
        } else {
            addVertex(*from)
        }
        edges = append(edges, edge)
        sum += edge.Weight
    }

    return edges, sum
}
