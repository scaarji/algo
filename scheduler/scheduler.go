package scheduler

import "sort"

type Scheduler struct {
	Compare Comparer
}

type sortable struct {
	Tasks   []Task
	compare Comparer
}

func (s sortable) Len() int {
	return len(s.Tasks)
}
func (s sortable) Less(i, j int) bool {
	return s.compare(s.Tasks[i], s.Tasks[j]) == 1
}
func (s sortable) Swap(i, j int) {
	s.Tasks[i], s.Tasks[j] = s.Tasks[j], s.Tasks[i]
}

func (s *Scheduler) Order(workload []Task) ([]Task, float64) {
	list := sortable{workload, s.Compare}
	sort.Sort(list)
    sum := 0.0
    length := 0.0
    for _, task := range list.Tasks {
        length += task.Length
        sum += task.Weight * length
    }
	return list.Tasks, sum
}
