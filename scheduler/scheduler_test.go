package scheduler

import "testing"

func TestScheduler(t *testing.T) {
	testCases := []struct {
		Compare  Comparer
		Workload []Task
		Expected float64
	}{
		{
			func(a, b Task) int {
				diff := a.Weight/a.Length - b.Weight/b.Length
				switch {
				case diff > 0:
					return 1
				case diff < 0:
					return -1
				}
				return 0
			},
			[]Task{
				Task{2.0, 2.0},
				Task{1.0, 3.0},
				Task{3.0, 1.0},
			},
			15,
		},
		{
			func(a, b Task) int {
				diff := a.Weight/a.Length - b.Weight/b.Length
				switch {
					case diff > 0:
						return 1
					case diff < 0:
						return -1
					case a.Weight > b.Weight:
						return 1
					case a.Weight < b.Weight:
						return -1
				}
				return 0
			},
			[]Task{
				Task{48, 14},
				Task{4, 90},
				Task{64, 22},
				Task{54, 66},
				Task{46, 6},
			},
			10548,
		},
		{
			func(a, b Task) int {
				diff := (a.Weight - a.Length) - (b.Weight - b.Length)
				switch {
					case diff > 0:
						return 1
					case diff < 0:
						return -1
					case a.Weight > b.Weight:
						return 1
					case a.Weight < b.Weight:
						return -1
				}
				return 0
			},
			[]Task{
				Task{48, 14},
				Task{4, 90},
				Task{64, 22},
				Task{54, 66},
				Task{46, 6},
			},
			11336,
		},
		{
			func(a, b Task) int {
				diff := a.Weight/a.Length - b.Weight/b.Length
				switch {
				case diff > 0:
					return 1
				case diff < 0:
					return -1
				}
				return 0
			},
			[]Task{
				Task{3.0, 5.0},
				Task{1.0, 2.0},
			},
			22,
		},
		{
			func(a, b Task) int {
				diff := a.Weight/a.Length - b.Weight/b.Length
				switch {
				case diff > 0:
					return 1
				case diff < 0:
					return -1
				}
				return 0
			},
			[]Task{
				Task{3.0, 5.0},
			},
			15,
		},
		{
			func(a, b Task) int {
				diff := (a.Weight - a.Length) - (b.Weight - b.Length)
				switch {
				case diff > 0:
					return 1
				case diff < 0:
					return -1
				}
				return 0
			},
			[]Task{
				Task{3.0, 5.0},
				Task{1.0, 2.0},
			},
			23,
		},
	}
	for _, testCase := range testCases {
		scheduler := Scheduler{testCase.Compare}

		_, sum := scheduler.Order(testCase.Workload)
		if sum != testCase.Expected {
			t.Errorf("Invalid sum %v, expected %v, %v", sum, testCase.Expected)
		}
	}
}
