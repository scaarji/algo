package keepsack

import "testing"

type Distance struct{from, to, weight int}

type TestCase struct {
    items []Item
    result int
    maxSize int
}

func TestKeepsack (t *testing.T) {
    testCases := []TestCase{
        TestCase{
            items: []Item{
                Item{10, 70},
                Item{10, 60},
                Item{10, 50},
                Item{10, 40},
                Item{10, 30},
                Item{10, 20},
                Item{10, 10},
                Item{10, 5},
                Item{10, 2},
                Item{10, 1},
            },
            result: 60,
            maxSize: 100,
        },
        TestCase{
            items: []Item{
                Item{7000, 1},
                Item{6000, 1},
                Item{5000, 1},
                Item{4000, 1},
                Item{3000, 1},
                Item{2000, 1},
                Item{1000, 1},
                Item{500, 1},
                Item{250, 1},
                Item{125, 1},
            },
            result: 27000,
            maxSize: 6,
        },
        TestCase{
            items: []Item{
                Item{125, 1},
                Item{250, 1},
                Item{500, 1},
                Item{1000, 1},
                Item{2000, 1},
                Item{3000, 1},
                Item{4000, 1},
                Item{5000, 1},
                Item{6000, 1},
                Item{7000, 1},
            },
            result: 27000,
            maxSize: 6,
        },
    }
    for testCaseIndex, testCase := range testCases {
        result := Solve(testCase.items, testCase.maxSize)
        if result != testCase.result {
            t.Errorf("Test case %v failed: Invalid result %v, expected %v", testCaseIndex, result, testCase.result)
        }
    }
}
