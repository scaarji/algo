package keepsack

type Item struct {
    Value int
    Size int
}

func Solve (items []Item, maxSize int) (price int) {
    var solve func (index int, maxSize int) (price int)
    cache := make(map[int]map[int]int)

    solve = func (index int, maxSize int) (price int) {
        // check if requested values exists in cache
        _, exists := cache[index]
        if !exists {
            cache[index] = make(map[int]int)
        } else {
            value, exists := cache[index][maxSize]
            if exists {
                return value
            }
        }
        item := items[index]
        var option1, option2 int

        if index != 0 {
            option1 += solve(index - 1, maxSize)
        }
        if item.Size <= maxSize {
            option2 += item.Value
            if index != 0 {
                option2 += solve(index - 1, maxSize - item.Size)
            }
        }
        if option1 > option2 {
            price = option1
        } else {
            price = option2
        }
        cache[index][maxSize] = price

        return price
    }

    return solve(len(items) - 1, maxSize)
}
