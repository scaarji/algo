package floydwarshall

import (
    al "bitbucket.org/scaarji/adjacencyList"
    "math"
)

type asps struct {
    pathHelpers [][]int
    distances [][]float64
}

func (a *asps) Init(graph al.AdjacencyList) bool {
    n := len(graph.Vertices)

    a.distances = make([][]float64, n)
    a.pathHelpers = make([][]int, n)

    for i := 0; i < n; i += 1 {
        a.distances[i] = make([]float64, n)
        a.pathHelpers[i] = make([]int, n)

        for j := 0; j < n; j += 1 {
            if i == j {
                a.distances[i][j] = 0
            } else {
                edge, exists := graph.GetConnectingEdge(i, j)
                if exists {
                    a.distances[i][j] = float64(edge.Weight)
                } else {
                    a.distances[i][j] = math.Inf(0)
                }
            }
        }
    }
    for k := 1; k < n; k++ {
        for i := 0; i < n; i++ {
            for j := 0; j < n; j++ {
                prev := a.distances[i][j]
                alternate := a.distances[i][k] + a.distances[k][j]
                if alternate < prev {
                    a.pathHelpers[i][j] = k
                    a.distances[i][j] = alternate
                } else {
                    a.distances[i][j] = prev
                }
            }
        }
    }
    for i := 0; i < n; i++ {
        if a.distances[i][i] != 0.0 {
            return false
        }
    }
    return true
}

func (a asps) GetPath(from, to int) (path []int) {
    // TODO: write method body
    return
}

func (a asps) GetDistance(from, to int) (distance float64) {
    distance = a.distances[from][to]
    return
}

func (a asps) GetShortestDistance() (from, to int, distance float64) {
    distance = math.Inf(0)

    n := len(a.distances)
    for i := 0; i < n; i++ {
        for j := 0; j < n; j++ {
            if i != j && a.distances[i][j] < distance {
                distance = a.distances[i][j]
                from = i
                to = j
            }
        }
    }
    return
}

func CreateASPS(graph al.AdjacencyList) (*asps, bool) {
    a := &asps{}
    ok := a.Init(graph)
    if ok {
        return a, true
    } else {
        return nil, false
    }
}
