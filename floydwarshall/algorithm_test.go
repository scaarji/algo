package floydwarshall

import (
    "testing"
    al "bitbucket.org/scaarji/adjacencyList"
)

type Distance struct{from, to, weight int}

type TestCase struct {
    nodesCnt int
    distances []Distance
    hasNegativeCycle bool
    shortestDistance float64
}

func TestFloydwarshall(t *testing.T) {
    testCases := []TestCase{
        TestCase{
            nodesCnt: 5,
            distances: []Distance{
                {1, 2, 2},
                {2, 3, 4},
                {3, 4, 8 },
                {4, 1, 16},
                {2, 4, -1},
                {4, 2, 8},
                {5, 5, 0},
                {2, 1, -3},
            },
            hasNegativeCycle: true,
            shortestDistance: -1.0,
        },
        TestCase{
            nodesCnt: 4,
            distances: []Distance{
                {1, 2, 2},
                {2, 3, 4},
                {3, 4, 8},
                {4, 1, 16},
                {2, 4, -1},
                {4, 2, 8},
            },
            hasNegativeCycle: false,
            shortestDistance: -1.0,
        },
        TestCase{
            nodesCnt: 5,
            distances: []Distance{
                {1, 2, 2},
                {2, 3, 4},
                {3, 4, 8},
                {4, 1, 16},
                {2, 4, -1},
                {4, 2, 8},
                {5, 5, 0},
            },
            hasNegativeCycle: false,
            shortestDistance: -1.0,
        },
        TestCase{
            nodesCnt: 5,
            distances: []Distance{
                {1, 2, 3},
                {1, 5, -4},
                {1, 3, 8},
                {2, 4, 1},
                {2, 5, 7},
                {3, 2, 4},
                {4, 1, 2},
                {4, 3, -5},
                {5, 4, 6},
            },
            hasNegativeCycle: false,
            shortestDistance: -5.0,
        },
    }
    for testCaseIndex, testCase := range testCases {
        list := al.BuildAdjacencyList()
        for i := 0; i < testCase.nodesCnt; i += 1 {
            list.AddVertex(al.Vertex{})
        }
        for _, edge := range testCase.distances {
            list.AddEdge(al.Edge{Weight: edge.weight}, edge.from - 1, edge.to - 1)
        }

        a, ok := CreateASPS(list)
        if testCase.hasNegativeCycle && ok {
            t.Errorf("Test case %v failed: Did not detect negative cost cycle", testCaseIndex)
            return
        }
        if !testCase.hasNegativeCycle {
            if a == nil {
                t.Errorf("Test case %v failed: Did not compute ASPS", testCaseIndex)
                return
            }
            _, _, shortestDistance := a.GetShortestDistance()
            if shortestDistance != testCase.shortestDistance {
                t.Errorf("Test case %v failed: Invalid shortest distance %v, expected %v", testCaseIndex, shortestDistance, testCase.shortestDistance)
            }
        }
    }
}
