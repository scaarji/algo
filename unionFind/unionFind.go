package unionFind

import _ "fmt"

type Set struct {
    Data interface{}
    rank int
    parent *Set
}

func MakeSet (data interface{}) *Set {
    set := Set{Data: data, rank: 1}
    set.parent = &set
    return &set
}

func Find (set *Set) *Set {
    if set.parent != set {
        parent := Find(set.parent)
        set.parent = parent
    }
    return set.parent
}

func Union (x, y *Set) *Set {
    xRoot := Find(x)
    yRoot := Find(y)

    if xRoot == yRoot {
        return xRoot
    }

    if xRoot.rank < yRoot.rank {
        xRoot.parent = yRoot
        yRoot.rank += xRoot.rank

        return yRoot
    } else {
        xRoot.rank += yRoot.rank
        yRoot.parent = xRoot

        return xRoot
    }
}
