package unionFind

import "testing"

func TestMakeSet (t *testing.T) {
    var set *Set

    data := 1
    set = MakeSet(data)
    if set.Data != data {
        t.Errorf("Invalid data saved %v, expected %v", set.Data, data)
        return
    }
    if set.parent != set {
        t.Error("Invalid parent pointer")
        return
    }
    if set.rank != 1 {
        t.Errorf("Invalid rank %v, expected %v", set.rank, 1)
        return
    }

    otherDate := "some string"
    set = MakeSet(otherDate)
    if set.Data != otherDate {
        t.Errorf("Invalid data saved %v, expected %v", set.Data, otherDate)
        return
    }
    if set.parent != set {
        t.Error("Invalid parent pointer")
        return
    }
    if set.rank != 1 {
        t.Errorf("Invalid rank %v, expected %v", set.rank, 1)
        return
    }
}

func TestFind (t *testing.T) {
    set := MakeSet(1)

    parent := Find(set)
    if set.parent != parent {
        t.Error("Invalid Find result")
        return
    }

    x := MakeSet(1)
    y := MakeSet(1)
    z := MakeSet(1)
    z.rank = 3

    Union(x, y)
    union := Union(z, x)
    parent = Find(y)
    if parent != union {
        t.Error("Invalid Find result %v, expected %v", parent, union)
        return
    }

}

func TestUnion (t *testing.T) {
    x := MakeSet(1)
    y := MakeSet(1)

    union := Union(x, y)

    if x.parent != union {
        t.Errorf("Parent not changed from %v to %v", *x.parent, union)
        return
    }
    if y.parent != union {
        t.Errorf("Parent not changed from %v to %v", *y.parent, union)
        return
    }
    if union.rank != 2 {
        t.Errorf("Invalid rank %v, expected %v", union.rank, 2)
        return
    }

    z := MakeSet(2)
    z.rank = 3

    newUnion := Union(z, union)
    if z.parent != newUnion {
        t.Errorf("Parent not changed from %v to %v", *z.parent, newUnion)
        return
    }
    if union.parent != newUnion {
        t.Errorf("Parent not changed from %v to %v", *union.parent, newUnion)
        return
    }
    if newUnion.rank != 5 {
        t.Errorf("Invalid rank %v, expected %v", newUnion.rank, 5)
        return
    }
    if y.parent == newUnion {
        t.Errorf("Leaf parents should not be updated")
        return
    }
}
